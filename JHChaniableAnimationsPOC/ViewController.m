//
//  ViewController.m
//  JHChaniableAnimationsPOC
//
//  Created by ALi on 21/07/15.
//  Copyright (c) 2015 ALi. All rights reserved.
//

#import "ViewController.h"
#import "UIView+JHChainableAnimations.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *animatedLabel;

@end

@implementation ViewController

- (IBAction)actionShowAnotherView:(UIButton *)sender {
    UIView *anotherView = [[NSBundle mainBundle] loadNibNamed:@"simpleView" owner:self options:nil][0];
    [self.view addSubview:anotherView];
}

- (void)viewDidLayoutSubviews {
    CGRect outOfScreen = self.animatedLabel.frame;
    outOfScreen.origin.y += 200;
    [self.animatedLabel setFrame:outOfScreen];
}

- (void)viewDidAppear:(BOOL)animated {
    self.animatedLabel.moveY(-200).spring.animate(1.4f);
}

@end
